FROM openjdk:8-jdk-alpine

COPY target/spring-boot-rest-swagger-example-0.0.1-SNAPSHOT.jar java-swagger-example.jar

ENTRYPOINT ["java", "-jar", "/java-swagger-example.jar"]
